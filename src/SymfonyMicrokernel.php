<?php

namespace Phops;

use \Phops\JSON;
use \Phops\URL;
use \Symfony\Bundle\FrameworkBundle\Console\Application as ConsoleApplication;
use \Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use \Symfony\Component\Config\Loader\LoaderInterface;
use \Symfony\Component\Config\Resource\GlobResource;
use \Symfony\Component\Console\Input\ArgvInput;
use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\Compiler\PassConfig;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use \Symfony\Component\Dotenv\Dotenv;
use \Symfony\Component\ErrorHandler\Debug;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

class SymfonyMicrokernel extends \Symfony\Component\HttpKernel\Kernel {

  use MicroKernelTrait;

  function __construct ($projectDir = '') {

    if ($projectDir)
      $this->projectDir = $projectDir;

    (new Dotenv(false))->populate([
      'DATA_PATH' => $this->getProjectDir() . '/data',
      'TEMP_PATH' => $this->getProjectDir() . '/var/temp',
    ]);

    if (!is_dir($_SERVER['TEMP_PATH']))
      mkdir($_SERVER['TEMP_PATH'], 0777, true);

    if (is_file(URL::follow($this->getProjectDir(), '.env.local')))
      (new Dotenv(false))->load(URL::follow($this->getProjectDir(), '.env.local'));

    $env = isset($_SERVER['APP_ENV']) && $_SERVER['APP_ENV'] ? $_SERVER['APP_ENV'] : 'dev';

    if (is_file(URL::follow($this->getProjectDir(), ".env.$env.local")))
      (new Dotenv(false))->load(URL::follow($this->getProjectDir(), ".env.$env.local"));

    $debug = filter_var(isset($_SERVER['APP_DEBUG']) ? $_SERVER['APP_DEBUG'] : 'true', FILTER_VALIDATE_BOOLEAN);

    if (!self::isSAPI()) {
      $input = new ArgvInput();
      if ($input->getParameterOption(['--env', '-e'], null, true) !== null)
        $env = $input->getParameterOption(['--env', '-e'], null, true);
      if ($input->hasParameterOption('--no-debug', true))
        $debug = false;
    }

    parent::__construct($env, $debug);
  }

  function run () {

    ini_set('memory_limit', '-1');
    set_time_limit(0);

    if ($this->isDebug()) {
      umask(0000);
      Debug::enable();
    }

    if (!self::isSAPI()) {
      $console = new ConsoleApplication($this);
      $console->run(new ArgvInput());
      return;
    }

    $request = Request::createFromGlobals();
    $response = $this->handle($request);
    $response->send();
    $this->terminate($request, $response);

  }

  function registerBundles (): iterable {
    return [
      new \Phops\SymfonyCORS\SymfonyCORSBundle(),
      new \Phops\SymfonyJSONRequest\SymfonyJSONRequestBundle(),
      new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
    ];
  }

  protected function getContainerBuilder () {
    $inlineBundleRegisters = $this->collectInlineBundleRegisters();
    $this->registerInlineBundles($inlineBundleRegisters);
    $builder = parent::getContainerBuilder();
    foreach ($inlineBundleRegisters as $inlineBundleRegister)
      $builder->fileExists($inlineBundleRegister['path'], true);
    $builder->getParameterBag()->set('kernel.bundles_registers_inline', $inlineBundleRegisters);
    return $builder;
  }

  protected function initializeContainer() {
    parent::initializeContainer();
    if (!$this->inlineBundlesRegistered)
      $this->registerInlineBundles($this->container->getParameter('kernel.bundles_registers_inline'));
  }

  protected $inlineBundlesRegistered = false;

  protected function registerInlineBundles ($inlineBundleRegisters) {
    foreach ($inlineBundleRegisters as $inlineBundleRegister) {
      $class = $inlineBundleRegister['class'];
      if (class_exists($class) && method_exists($class, 'registerBundles'))
        foreach ($class::registerBundles() as $bundle) {
          $name = $bundle->getName();
          if (isset($this->bundles[$name]))
            throw new \LogicException(sprintf('Trying to register two bundles with the same name "%s".', $name));
          $this->bundles[$name] = $bundle;
        }
    }
    $this->inlineBundlesRegistered = true;
  }

  protected function collectInlineBundleRegisters () {
    $inlineBundleRegisters = [];
    foreach ($this->getComposerServices() as $serviceNamespace => $serviceResource) {
      $resource = new GlobResource(URL::follow($this->getProjectDir(), $serviceResource), '', true);
      foreach ($resource as $path => $_) {
        $class = $serviceNamespace . ltrim(str_replace('/', '\\', substr(
          preg_replace('/\\.php\z/i', '', $path),
          strlen(URL::follow($this->getProjectDir(), $serviceResource)),
        )), '\\');
        if (class_exists($class) && method_exists($class, 'registerBundles'))
          $inlineBundleRegisters[] = [
            'class' => $class,
            'path' => $path,
          ];
      }
    }
    return $inlineBundleRegisters;
  }

  protected function build (ContainerBuilder $builder): void {

    parent::build($builder);

    \Phops\SymfonyCheck\CheckCommand::buildContainer($builder);

    if ($this->isDebug())
      $builder->addCompilerPass(new class () implements CompilerPassInterface {
        function process (ContainerBuilder $builder): void {
          $logger = new Definition(\Symfony\Component\HttpKernel\Log\Logger::class);
          $logger->setArguments([null, 'php://stderr']);
          $builder->setDefinition('logger', $logger);
        }
      });

    $builder->addCompilerPass(new class () implements CompilerPassInterface {
      function process (ContainerBuilder $builder): void {
        foreach ($builder->getServiceIds() as $id)
          if (class_exists($id) && property_exists($id, 'serviceTags'))
            foreach ($id::$serviceTags as $tag)
              if (!$builder->getDefinition($id)->hasTag($tag))
                $builder->getDefinition($id)->addTag($tag);
      }
    }, PassConfig::TYPE_BEFORE_OPTIMIZATION, -10000);

    foreach ($this->getComposerServices() as $serviceNamespace => $serviceResource) {
      $resource = new GlobResource(URL::follow($this->getProjectDir(), $serviceResource), '', true);
      foreach ($resource as $path => $_) {
        $class = $serviceNamespace . ltrim(str_replace('/', '\\', substr(
          preg_replace('/\\.php\z/i', '', $path),
          strlen(URL::follow($this->getProjectDir(), $serviceResource)),
        )), '\\');
        if (class_exists($class) && method_exists($class, 'buildContainer'))
          $class::buildContainer($builder);
      }
    }

  }

  protected function configureContainer (ContainerConfigurator $container, LoaderInterface $loader): void {

    $loader->load(function (ContainerBuilder $builder) {
      $builder->loadFromExtension('framework', [
        'router' => [
          'utf8' => true,
        ],
      ]);
    });

    foreach ($this->getComposerServices() as $serviceNamespace => $serviceResource) {
      $service = $container->services()->load(
        $serviceNamespace,
        URL::follow($this->getProjectDir(), $serviceResource),
      );
      $service->autowire();
      $service->autoconfigure();
      $service->exclude(URL::follow($this->getProjectDir(), $serviceResource) . '/Migrations/');
    }

    foreach ($this->getComposerServices() as $serviceNamespace => $serviceResource) {
      $resource = new GlobResource(URL::follow($this->getProjectDir(), $serviceResource), '', true);
      foreach ($resource as $path => $_) {
        $class = $serviceNamespace . ltrim(str_replace('/', '\\', substr(
          preg_replace('/\\.php\z/i', '', $path),
          strlen(URL::follow($this->getProjectDir(), $serviceResource)),
        )), '\\');
        if (class_exists($class) && method_exists($class, 'configureContainer'))
          $class::configureContainer($container, $loader);
      }
    }

  }

  protected function configureRoutes (RoutingConfigurator $routes): void {
    foreach ($this->getComposerServices() as $serviceNamespace => $serviceResource)
      $routes->import(URL::follow($this->getProjectDir(), $serviceResource), 'annotation');
  }

  protected function getComposerServices () {
    if (!is_file($this->getProjectDir() . '/' . (trim(getenv('COMPOSER')) ?: './composer.json')))
      return;
    $composerData = JSON::decode(file_get_contents(
      $this->getProjectDir() . '/' . (trim(getenv('COMPOSER')) ?: './composer.json')
    ));
    $composerSections = ['autoload'];
    if ($this->getEnvironment() != 'prod')
      $composerSections[] = 'autoload-dev';
    foreach ($composerSections as $composerSection)
      if (isset($composerData[$composerSection]) && isset($composerData[$composerSection]['psr-4']))
        foreach ($composerData[$composerSection]['psr-4'] as $serviceNamespace => $serviceResource)
          yield $serviceNamespace => $serviceResource;
  }

  protected $projectDir;

  function getProjectDir () {

    if ($this->projectDir === null && self::class == get_class($this)) {
      $dir = $rootDir = dirname(__DIR__, 2);
      while (!is_file($dir . '/composer.json')) {
        if ($dir === dirname($dir))
          return $this->projectDir = $rootDir;
        $dir = dirname($dir);
      }
      $this->projectDir = $dir;
    }

    if ($this->projectDir === null)
      $this->projectDir = parent::getProjectDir();

    return $this->projectDir;

  }

  function isBooted () {
    return $this->booted;
  }

  static function isSAPI () {
    return !in_array(\PHP_SAPI, ['cli', 'phpdbg', 'embed']);
  }

}
