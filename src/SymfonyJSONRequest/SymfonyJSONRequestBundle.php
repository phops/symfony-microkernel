<?php

namespace Phops\SymfonyJSONRequest;

use \Phops\SymfonyJSONRequest\SymfonyJSONRequestEventSubscriber;
use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\Compiler\PassConfig;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;

class SymfonyJSONRequestBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle implements CompilerPassInterface {

  function build (ContainerBuilder $builder) {
    $builder->addCompilerPass($this, PassConfig::TYPE_BEFORE_REMOVING, 1);
  }

  function process (ContainerBuilder $builder): void {
    $symfonyJSONRequestEventSubscriber = new Definition(SymfonyJSONRequestEventSubscriber::class);
    $symfonyJSONRequestEventSubscriber->setAutowired(true);
    $symfonyJSONRequestEventSubscriber->setAutoconfigured(true);
    $symfonyJSONRequestEventSubscriber->addTag('kernel.event_listener', [
      'event' => 'kernel.request',
      'method' => 'onKernelRequest',
    ]);
    $builder->setDefinition(SymfonyJSONRequestEventSubscriber::class, $symfonyJSONRequestEventSubscriber);
  }

}
