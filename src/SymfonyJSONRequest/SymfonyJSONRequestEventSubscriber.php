<?php

namespace Phops\SymfonyJSONRequest;

use \Phops\JSON;
use \Phops\JSONException;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\Event\RequestEvent;
use \Symfony\Component\HttpKernel\KernelEvents;

class SymfonyJSONRequestEventSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface {

  function onKernelRequest (RequestEvent $event) {

    // @see http://silex.sensiolabs.org/doc/cookbook/json_request_body.html#parsing-the-request-body
    if (strpos($event->getRequest()->headers->get('Content-Type'), 'application/json') !== 0)
      return;

    try {
      if (empty($event->getRequest()->getContent()))
        return;
      $jsonDecoded = JSON::decode($event->getRequest()->getContent());
      $event->getRequest()->request->replace(is_object($jsonDecoded) ? $jsonDecoded->getArrayCopy() : $jsonDecoded);
    } catch (JSONException $exception) {
      $event->setResponse(new Response($exception->getMessage(), Response::HTTP_BAD_REQUEST));
    }

  }

  static function getSubscribedEvents () {
    return [
      KernelEvents::REQUEST => 'onKernelRequest',
    ];
  }

}
