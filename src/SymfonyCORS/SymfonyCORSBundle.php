<?php

namespace Phops\SymfonyCORS;

use \Phops\SymfonyCORS\SymfonyCORSController;
use \Phops\SymfonyCORS\SymfonyCORSEventSubscriber;
use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\Compiler\PassConfig;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\Routing\Route;

class SymfonyCORSBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle implements CompilerPassInterface {

  function build (ContainerBuilder $builder) {

    $symfonyCORSController = new Definition(SymfonyCORSController::class);
    $symfonyCORSController->setAutowired(true);
    $symfonyCORSController->setAutoconfigured(true);
    $builder->setDefinition(SymfonyCORSController::class, $symfonyCORSController);

    $builder->addCompilerPass($this, PassConfig::TYPE_BEFORE_REMOVING, 1);
  }

  function process (ContainerBuilder $builder): void {
    $symfonyCORSEventSubscriber = new Definition(SymfonyCORSEventSubscriber::class);
    $symfonyCORSEventSubscriber->setAutowired(true);
    $symfonyCORSEventSubscriber->setAutoconfigured(true);
    //$symfonyCORSEventSubscriber->addTag('kernel.event_listener', [
    //  'event' => 'kernel.request',
    //  'method' => 'onKernelRequest',
    //  'priority' => 1024,
    //]);
    $symfonyCORSEventSubscriber->addTag('kernel.event_listener', [
      'event' => 'kernel.response',
      'method' => 'onKernelResponse',
      'priority' => -1024,
    ]);
    $builder->setDefinition(SymfonyCORSEventSubscriber::class, $symfonyCORSEventSubscriber);
  }

  function boot () {
    $this->container->get('router')->getRouteCollection()->add('/{path<.*>}', new Route(
      '/{path<.*>}', ['_controller' => SymfonyCORSController::class . '::options'], [], [], '', [], ['OPTIONS']
    ), -1024);
  }

}
