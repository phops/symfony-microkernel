<?php

namespace Phops\SymfonyCORS;

use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpKernel\Event\RequestEvent;
use \Symfony\Component\HttpKernel\Event\ResponseEvent;
use \Symfony\Component\HttpKernel\KernelEvents;

class SymfonyCORSEventSubscriber implements \Symfony\Component\EventDispatcher\EventSubscriberInterface {

  /*
  function onKernelRequest (RequestEvent $event) {

    return;

    if (!$event->isMasterRequest())
      return;

    if (!$event->getRequest()->isMethod('OPTIONS'))
      return;

    $event->setResponse(new Response('', Response::HTTP_NO_CONTENT, [
      'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
    ]));

  }
  /**/

  function onKernelResponse (ResponseEvent $event) {

    $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
    $event->getResponse()->headers->set('Access-Control-Allow-Headers', '*');
    $event->getResponse()->headers->set(
      'Access-Control-Expose-Headers',
      implode(', ', array_keys($event->getResponse()->headers->all()))
    );

    /*

    if (false)
    if ($event->getRequest()->isMethod('OPTIONS')) {

      //if ($event->getResponse()->getStatusCode() != 404)
      //  return;

      $event->getResponse()->setStatusCode(204);
      $event->getResponse()->setContent('');
      $event->getResponse()->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

      return;

    }

    $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
    $event->getResponse()->headers->set('Access-Control-Allow-Headers', '*');
    $event->getResponse()->headers->set(
      'Access-Control-Expose-Headers',
      implode(', ', array_keys($event->getResponse()->headers->all()))
    );

    return;

    if (!$event->getRequest()->isMethod('OPTIONS'))
      return;

    if ($event->getResponse()->getStatusCode() != 404)
      return;

    $event->getResponse()->setStatusCode(204);
    $event->getResponse()->setContent('');
    $event->getResponse()->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    return;
    var_dump("A");
    exit;
    $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
    $event->getResponse()->headers->set('Access-Control-Allow-Headers', '*');
    $event->getResponse()->headers->set(
      'Access-Control-Expose-Headers',
      implode(', ', array_keys($event->getResponse()->headers->all()))
    );
    /**/
  }

  static function getSubscribedEvents () {
    return [
      //KernelEvents::REQUEST => 'onKernelRequest',
      KernelEvents::RESPONSE => ['onKernelResponse', -1024],
    ];
  }

}
