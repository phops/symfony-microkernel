<?php

namespace Phops\SymfonyCORS;

use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\Routing\Annotation\Route;

class SymfonyCORSController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController {

  /**
   * @Route("/{path<.*>}", methods={"OPTIONS"}, priority=-1024)
   */
  function options (Request $request) {
    return new Response('', Response::HTTP_NO_CONTENT, [
      'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
    ]);
  }

}
