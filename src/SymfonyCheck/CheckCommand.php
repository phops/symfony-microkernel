<?php

namespace Phops\SymfonyCheck;

use \Psr\Container\ContainerInterface;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\Compiler\PassConfig;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Reference;
use \Symfony\Component\Console\Formatter\OutputFormatter;

class CheckCommand extends \Symfony\Component\Console\Command\Command {

  static function buildContainer (ContainerBuilder $builder) {
    $builder->addCompilerPass(new class () implements CompilerPassInterface {
      function process (ContainerBuilder $builder): void {
        $definition = new Definition(CheckCommand::class);
        foreach ($builder->findTaggedServiceIds('kernel.env_check', true) as $id => $tags)
          $builder->getDefinition($id)->setPublic(true);
        $definition->setArguments([new Reference('service_container'), array_map(function ($id) {
          return $id;
        }, array_keys($builder->findTaggedServiceIds('kernel.env_check', true)))]);
        $definition->setAutowired(true);
        $definition->setAutoconfigured(true);
        $definition->addTag('console.command');
        $builder->setDefinition(CheckCommand::class, $definition);
      }
    }, PassConfig::TYPE_BEFORE_REMOVING, 32);
  }

  protected ContainerInterface $container;

  protected $checks = [];

  function __construct (ContainerInterface $container, $checks) {
    $this->container = $container;
    $this->checks = $checks;
    parent::__construct();
  }

  protected function configure () {
    $this
      ->setName('check')
      ->setDescription('Check that environment is fully setup.')
    ;
  }

  protected function execute (InputInterface $input, OutputInterface $output) {

    $failures = 0;

    $output->writeln('');
    $output->writeln('Checking ...');
    $output->writeln('');

    foreach ($this->checks as $checkID) {
      try {
        $check = $this->container->get($checkID);
        $check->setOutput($output);
        $check->run($output);
      } catch (\Exception $e) {
        $output->writeln(' <error>' . OutputFormatter::escape($e->getMessage()) . '</error>.');
        ++$failures;
      }
    }

    $output->writeln('');
    $output->writeln('Done, ' . ($failures > 0 ? '<error>' . $failures . '</error>' :
      '<info>' . $failures . '</info>') . ' failure(s) found.');
    $output->writeln('');

    return 0 == $failures ? 0 : 1;

  }

}
