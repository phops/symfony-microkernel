<?php

namespace Phops\SymfonyMicrokernel;

use \Symfony\Component\Console\Formatter\OutputFormatter;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\NullOutput;
use \Symfony\Component\Console\Output\OutputInterface;
use \Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use \Symfony\Component\DependencyInjection\Compiler\PassConfig;
use \Symfony\Component\DependencyInjection\ContainerBuilder;
use \Symfony\Component\DependencyInjection\Definition;
use \Symfony\Component\DependencyInjection\Reference;

class Worker {

  static function buildContainer (ContainerBuilder $builder) {

    $builder->addCompilerPass(new class (static::class) implements CompilerPassInterface {
      function __construct ($class) {
        $this->class = $class;
      }
      function process (ContainerBuilder $builder): void {
        $class = $this->class;
        $definition = new Definition(\Phops\SymfonyMicrokernel\Command::class);
        $definition->setArguments(['worker:' . $class::workerName()]);
        $definition->addMethodCall('setCode', [[new Reference($class), 'executeCommand']]);
        $definition->setAutowired(true);
        $definition->setAutoconfigured(true);
        $definition->addTag('console.command');
        $builder->setDefinition($class . '.command', $definition);
      }
    }, PassConfig::TYPE_BEFORE_REMOVING, 32);

    return;

    if (self::class != static::class)
      return;
    $builder->addCompilerPass(new class () implements CompilerPassInterface {
      function process (ContainerBuilder $builder): void {
        foreach ($builder->getServiceIds() as $id)
          if (class_exists($id) && is_a($id, Worker::class, true) && !is_a(Worker::class, $id, true)) {
            $definition = new Definition(\Phops\SymfonyMicrokernel\Command::class);
            #$name = $id;
            #if (strpos($name, '\\') !== false)
            #  $name = substr($name, -strrpos($name, '\\'));
            #if (substr(strtolower($name), -strlen('worker')) == 'worker')
            #  $name = substr($name, 0, strlen($name) -strlen('worker'));
            #$name = preg_replace_callback('/\A[A-Z]/', function ($match) { return strtolower($match[0]); }, $name);
            #$name = preg_replace_callback('/[A-Z]/', function ($match) { return '-' . strtolower($match[0]); }, $name);
            #if (property_exists($id, 'name'))
            #  $name = $id::$name;
            #var_dump($name); exit;
            #$definition->setFactory(function () { return new \Symfony\Component\Console\Command\Command(); });
            #$definition->setArguments(['worker:' . $id::$name]);
            #$definition->setArguments(['worker:' . $name]);
            $definition->setArguments(['worker:' . $id::workerName()]);
            $definition->addMethodCall('setCode', [[new Reference($id), 'executeCommand']]);
            #$definition->setArguments([new Reference('doctrine.migrations.dependency_factory')]);
            $definition->setAutowired(true);
            $definition->setAutoconfigured(true);
            #$definition->setPublic(true);
            $definition->addTag('console.command');
            #$builder->setDefinition($id . '.command', $definition);
            $builder->setDefinition($id . '.command', $definition);
          }
            #if (!$builder->getDefinition($id)->hasTag('worker'))
            #  $builder->getDefinition($id)->addTag('worker');

            #foreach ($id::$serviceTags as $tag)
            #  if (!$builder->getDefinition($id)->hasTag($tag))
            #    $builder->getDefinition($id)->addTag($tag);

        /*
        $definition = new Definition(CheckCommand::class);
        foreach ($builder->findTaggedServiceIds('kernel.env_check', true) as $id => $tags)
          $builder->getDefinition($id)->setPublic(true);
        $definition->setArguments([new Reference('service_container'), array_map(function ($id) {
          return $id;
        }, array_keys($builder->findTaggedServiceIds('kernel.env_check', true)))]);
        $definition->setAutowired(true);
        $definition->setAutoconfigured(true);
        $definition->addTag('console.command');
        $builder->setDefinition(CheckCommand::class, $definition);
        /**/
      }
    }, PassConfig::TYPE_BEFORE_REMOVING, 32);
  }

  static function workerName () {
    $name = static::class;
    if (strpos($name, '\\') !== false)
      $name = substr($name, strrpos($name, '\\') + 1);
    if (substr(strtolower($name), -strlen('worker')) == 'worker')
      $name = substr($name, 0, strlen($name) -strlen('worker'));
    $name = preg_replace_callback('/\A[A-Z]/', function ($match) { return strtolower($match[0]); }, $name);
    $name = preg_replace_callback('/[A-Z]/', function ($match) { return '-' . strtolower($match[0]); }, $name);
    if (property_exists(static::class, 'name'))
      $name = static::$name;
    return $name;
  }

  protected OutputInterface $output;

  function __construct () {
    $this->output = new NullOutput();
  }

  function executeCommand (InputInterface $input, OutputInterface $output) {

    $this->output = $output;

    $output->writeln('Starting worker <fg=cyan>' . OutputFormatter::escape(static::workerName()) . '</>...');

    $lastRun = 0;

    while (true) {
      $nextRun = $this->schedule($lastRun);
      #var_dump($nextRun);
      $nextRunSleep = $nextRun - microtime(true);
      if ($nextRunSleep < 0)
        $nextRunSleep = 0;
      #var_dump($nextRunSleep);
      #var_dump(date_create()->setTimestamp($lastRun)->format('c'));
      #var_dump(date_create()->setTimestamp(microtime(true))->format('c'));
      #var_dump(date_create()->setTimestamp($nextRun)->format('c'));
      $output->writeln(
        'Next run in <fg=cyan>' . OutputFormatter::escape(number_format($nextRunSleep, 2)) . '</> seconds...'
      );
      usleep($nextRunSleep * 1000000);
      #var_dump($nextRun);
      #exit;
      try {
        $output->writeln('Running...');
        $t = microtime(true);
        $this->run();
        $runTime = microtime(true) - $t;
        $output->writeln(
          'Run complete in <fg=cyan>' . OutputFormatter::escape(number_format($runTime, 2)) . '</> second(s)'
        );
        #sleep(60);
      } catch (\Throwable $t) {
        $output->writeln('<error>' . OutputFormatter::escape($t) . '</error>');
        #$output->writeln('Retry in 60 seconds...');
        #sleep(60);
      } finally {
        $lastRun = microtime(true);
      }
    }

    return 0;

  }

  protected function schedule ($lastRun) {
    #return floor($lastRun ?: time()) - floor($lastRun ?: time()) % 60 + 60;
    return $lastRun + 60;
  }

  public function run () {
    $this->execute($this->output);
  }

}
