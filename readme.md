
Phops Symfony Microkernel
=========================


Basic usage
-----------

```bash
composer require phops/symfony-microkernel
```

```php
<?php

require __DIR__ . '/vendor/autoload.php';

\App\Kernel::run();

```


License
-------

Phops JSON is licensed under the [MIT license](/license.txt).
